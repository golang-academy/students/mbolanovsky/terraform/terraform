# This is my first EC2 Instance
resource "aws_instance" "web" {
  ami             = var.image
  instance_type   = var.instance
  user_data       = file("postinstall.sh")
  security_groups = ["${aws_security_group.allow_http_ssh.name}"]

  tags = {
    Owner = var.owner
    Name  = "web"
  }
}

