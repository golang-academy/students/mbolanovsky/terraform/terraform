#!/bin/bash
username=tux
# Create User
useradd -s /bin/bash -c "Admin" -m ${username}
echo "Passw0rd" | passwd --stdin ${username}
# Set sudo
echo "${username} ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/${username}/.ssh
cat <<EOF | tee -a /home/${username}/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMRDX8m5fb/2Ri6NhXeWVE8sWyJRuMZ8+JCQ1b+m4YnH eddsa-key-20230208
EOF
# Set proper permissions
chown -R ${username}:${username} /home/${username}/.ssh
chmod 700 /home/${username}/.ssh
chmod 600 /home/${username}/.ssh/authorized_keys
##
apt install -y nginx
echo "Welcome from Terraform managed EC2" > /var/www/html/index.html
